# forkonomy() 

A co-writing-forking-performing project by Soon Winnie and Lee Tzu Tung

## setup()

- staging()
- intro&why()

## start()
   
- inputData(project_info, why_CC)
- what'sCC()

## Exercise

- generateQuestions()
- generateDiscussion()

## Source (code)
[RunMe](https://editor.p5js.org/siusoon/present/k6xUx6nbc)
[Full source code](https://editor.p5js.org/siusoon/sketches/k6xUx6nbc)

```javascript
let sea_video;
let questions = ["why do we concern about freedom? What forms of particular freedom are we subscribed to?",
  "How can we have creative commons but still able to sustain financially?",
  "What it means by, or how could we imagine, forking economy as an ongoing gene space? ",
  "How could we fork the forktopia?",
  "Would forking economy help to save those pressing issues, such as having constant threats in maintaining autonomy, in both Taiwan and Hong Kong?",
  "Does econonmy exists before 18th century Taiwan, or just piracy?",
  "Why has power in the West assumed the form of an “econonmy”, of a government encompasses all men, productive activities and things?",
  "What’s left without the conclusion?",
  "What’s the possibility of a forked historical text?"
];
let val = 0;


function setup() {
  createCanvas(1280,1300);
  sea_video = createVideo(['video.mp4']);
  sea_video.hide();
}
function mousePressed() {
  sea_video.loop();
}

function keyPressed() {
  //spacebar - check here: http://keycode.info/
  if (keyCode === 81) {
    val += 1;
    if (val % 2 != 0) {
      noLoop();
    } else {
      loop();
    };
  }
}


function draw() {
  background(0);
  tint(255, 200); // make the video partially transparent without changing the color
  image(sea_video, 0, 0); // draw the video frame to canvas
  forkonomy();
}

function forkonomy() {
  let question = random(questions);
  fill(220, 226, 229);
  textSize(width * 0.025);
  textLeading(width * 0.04);
  text('forkonomy() 岔經濟（）: a queer ocean of freedom and the sea of commonwealth', width / 20, height / 1.7);
  textSize(width * 0.02);
  text(question, width / 20, height / 1.7 + width * 0.04, width / 1.1);
}

```
## While()
    
- merge()
- fork()

## Reference
- Kingdom of Piracy: http://yukikoshikata.com/kingdom-of-piracy/#en
- Submarine cable map: https://www.submarinecablemap.com/
- traceroute-online: https://traceroute-online.com/
- creating commons: http://creatingcommons.zhdk.ch/
- 1st prototype by Artemis: https://pzwiki.wdka.nl/mediadesign/User:Artemis_gryllaki/Prototyping
- Made with creative Commons: https://creativecommons.org/wp-content/uploads/2017/04/made-with-cc.pdf
- Debatty, Regine, and Rosa Menkman. "Glitch Moment/ums-From Tech Accident to Artistic Expression." We Make Money Not Art (blog). 23 July 2013. https://we-make-money-not-art.com/glitch_momentums/
- [On lockdown, Hong Kong activists are protesting in Animal Crossing](https://www.wired.co.uk/article/animal-crossing-hong-kong-protests-coronavirus)
- [The Quiet Revolution of Animal Crossing](https://www.theatlantic.com/family/archive/2020/04/animal-crossing-isnt-escapist-its-political/610012/) by Ian Bogost (Apr 2020)
- links on south china live streaming: https://zoom.earth/#view=14.56,113.3,6z/date=2020-09-03,17:00,+2/layers=labels,fires,crosshairs

## Notes: 
- dialogues/conversation with audience
- drawing: https://demo.webstrates.net/shy-termite-28/
- Miro drawing: https://miro.com/app/board/o9J_kwwJpOw=/

--- 
# Description

In contemporary economical expansion, America federal and banks, giant tech corporations have managed to maintain their colonial forces to govern the accessibility and distribution of resources. The rise of China economic power, though operating in a different regime, also follows and adopts similar capitalistic, centralized and advanced (computable) machines, in which the invisible hand exploits ecologies and alienates labors. The economy trade bloc further demarcates the nowaday island states and territories of the Pacific.

However, many people have lived and traversed through the sea, between islands, coastlines, and cities for over two thousand years. Taiwan as the northeast Austronesians and Hong Kong 蜑家 Dang-Chia as “ocean peoples” viewed the world as “a sea of islands” rather than “places along the continent”. With the history and development of port infrastructure, it is not surprised that bartering and piracy on material and intangible wealth were the precursors to the monetary system.

Through Forkonomy(), we are interested in rethinking the politics of our contemporary economic and technical-cultural systems. By employing free and open source software and decentralized protocols, we set the participatory project as **a commoning ship for people of the pacific who want to queer the matters of hierarchies, ownership, gendered labor division, as well as to fight against the constant threats of maintaining a high degree of autonomy regarding the land and the sea.** Forknomy() shall sail the economy and autonomy into a queer ocean of freedom and the sea of commonwealth. 

# Draft

- why asking question: 
> "I feel that many people have lost the ability to formulate questions- this generation has become good at researching and finding answers or creating new datasets: in university, in the library, or on google ("the internet") we are conditioned to find and formulate answers [...]. Personally I think that one of the most important roles of art is to create problems that provoke curiosity-the impluse to investigate the limits of what we know and to ask questions." (Debatty and Menkman 2013)

- <img src="https://i.imgur.com/fCjH7qB.jpg" width="450"/>

- traceroute()

# Personal Questions/Logs

- 2020.08.04 00:56 Lee Tzu Tung:

Today I am taking a workshop on deliberative democracy at Tai-Chung,  commissioned by our culture department. I thought, ARThon, was a platform I wanted to make, for artists to have a cross-disciplinary, deliberative place to collaborate. For me, our project, Forkonomy() continues this spirit, actively re-consider and open-sourcing the old/current/upcoming contemporary art projects, while using open-sourcing as an advantage, encourages artists to develop additional and creative income sources.

All of these above is an ocean that I haven't sailed on before. It is challenging because all these above are all the things that I was afraid of. Knowing that there is the Chicago School of Economy, Freidman, Nash and many other economic scholars who are licensed to shaped the current world, and I have tried escape and I can never be in a world exempt from it -- I now want to cautiously sail on the current system and seeking alternative routes to ... boil up the ocean.

My father had an MBA in Chicago, which is something marvelous and rare among his generation. After he came back, he wanted to open a business consulting firm but failed. He then opened various stores and restaurants which none of them last. Provoked by the series of failures, he got depression, along with alcohol abuse. 

That is the prologue for the work Translation, the Chauvinistic father later blamed his problem on my mom for having a better job and a strong women, and blamed me, for being as a queer daughter, who can never inherit Lee family's spirit. The result of his business failures became the base note of my childhood. In my family's study room, there is a wall filled with business management books, all these results in nothing -- it was a wall of disgusting books for me, which created these stupid shitty 
entrepreneurial dreams in my father's head.

Art seems to be a place to escape. No one seems to care about money, everyone, as I did, hates it. The richer the dirtier. It is years after, when I see my art colleagues one by one facing money problems, that I realized money has turned back, chocking my art - the true and only love I so religiously devoted in. For ARThon, I started to learn how to draft a business model,  I picked up one of the books on that wall of the study room. I realized that I have to deal with money or, deeply speaking, to deal with the shadow that left by my father --- I have to learn, how to forgive my father -- his patriarchal backgrounds and his unfufilled dreams -- I know I have to learn where did he came from and where did he fail, no longer avoiding the questions, that I can truly get rid of that shadow, and not blaming him, again -- like he blaming me. That is also the way, that I can truly prevent myself, from repeating his mistake. 


- 2020.08.04 02:19 Lee Tzu Tung: @ Winnie, Do you also have a story about money?
- 2020.08.06 12:00 Winnie: @tzutung: Thanks for sharing your story :) had a really good chat, thank you! 

 



