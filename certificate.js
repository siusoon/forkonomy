ocean CodeContract
NanHai extends ocean

queer(ocean):
  get(ocean);
  return ocean;

transfer(ocean, buyer, seller, licensing):
  get(ocean);
  ocean.owner = buyer;
  ocean.transaction = seller;
  ocean.terms = licensing;
  put(ocean);
  return ocean;

update(ocean, status, units):
  get(ocean);
  ocean.type = properties.status;
  ocean.form = properties.units;
  put(ocean);
  return ocean;

maintain(ocean, behaviors, activities):
  get(ocean);
  ocean.CodeofConduct = behaviors;
  ocean.CodeofConduct = activities;
  put(ocean);
  return ocean;
